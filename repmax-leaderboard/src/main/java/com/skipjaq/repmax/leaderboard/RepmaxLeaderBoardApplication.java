package com.skipjaq.repmax.leaderboard;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author Rob Harrop
 */
@SpringBootApplication
@Controller
@EnableEurekaClient
public class RepmaxLeaderBoardApplication {

    public static void main(String[] args) {
        SpringApplication.run(RepmaxLeaderBoardApplication.class, args);
    }

    @PostMapping("/lifts")
    public HttpEntity<Void> recordLift(@RequestParam String exerciseName, @RequestParam String lifterName, @RequestParam int reps, @RequestParam double weight) {
        System.out.printf("%s did %d reps of %s at %f\n", lifterName, reps, exerciseName, weight);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
