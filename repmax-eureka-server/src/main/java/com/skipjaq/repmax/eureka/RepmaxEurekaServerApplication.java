package com.skipjaq.repmax.eureka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

/**
 * @author Rob Harrop
 */
@SpringBootApplication
@EnableEurekaServer
public class RepmaxEurekaServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(RepmaxEurekaServerApplication.class, args);
    }
}
