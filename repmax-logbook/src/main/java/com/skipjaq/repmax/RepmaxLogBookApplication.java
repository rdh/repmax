package com.skipjaq.repmax;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
public class RepmaxLogBookApplication {

    public static void main(String[] args) {
        SpringApplication.run(RepmaxLogBookApplication.class, args);
    }
}
