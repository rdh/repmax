package com.skipjaq.repmax;

import com.skipjaq.repmax.model.Lift;
import com.skipjaq.repmax.model.Lifter;
import com.skipjaq.repmax.model.Workout;

import java.util.List;
import java.util.UUID;

/**
 * @author Rob Harrop
 */
public interface LogBookService {

    List<Lifter> allLifters();

    Lifter lifterById(UUID id);

    List<Workout> workoutsByLifter(UUID lifterID);

    List<Lift> liftsByWorkout(UUID workoutID);

    void store(Workout workout);

    /**
     * Adds a lift to a workout if that workout isn't already completed.
     *
     * @param workoutID   the ID of the workout
     * @param description the description of the lift
     * @param sets        how many sets
     * @param reps        how many reps per set
     * @param weight      at what weight?
     * @return the <code>Lift</code> that was created.
     */
    Lift addLiftToWorkout(UUID workoutID, String description, int sets, int reps, double weight) throws WorkoutFinishedException;

    class WorkoutFinishedException extends Exception {

        public WorkoutFinishedException(UUID uuid) {
            super(String.format("Workout is already finished: %s", uuid));
        }
    }
}
