package com.skipjaq.repmax.cassandra;


import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Session;
import com.datastax.driver.mapping.Mapper;
import com.datastax.driver.mapping.MappingManager;
import com.datastax.driver.mapping.Result;
import com.datastax.driver.mapping.annotations.Accessor;
import com.datastax.driver.mapping.annotations.Param;
import com.datastax.driver.mapping.annotations.Query;
import com.skipjaq.repmax.LeaderBoardApi;
import com.skipjaq.repmax.LogBookService;
import com.skipjaq.repmax.model.Lift;
import com.skipjaq.repmax.model.Lifter;
import com.skipjaq.repmax.model.Workout;

import java.util.List;
import java.util.UUID;

final class CassandraLogBookService implements LogBookService {

    private final LogBookAccessor accessor;

    private final Mapper<Workout> workoutMapper;

    private final Mapper<Lift> liftMapper;

    private final LeaderBoardApi leaderBoardApi;

    private final Session session;

    public CassandraLogBookService(Session session, LeaderBoardApi leaderBoardApi) {
        MappingManager mappingManager = new MappingManager(session);
        this.workoutMapper = mappingManager.mapper(Workout.class);
        this.liftMapper = mappingManager.mapper(Lift.class);
        this.leaderBoardApi = leaderBoardApi;
        this.accessor = mappingManager.createAccessor(LogBookAccessor.class);
        this.session = session;
    }

    @Override
    public List<Lifter> allLifters() {
        return this.accessor.allLifters().all();
    }


    @Override
    public Lifter lifterById(UUID id) {
        return this.accessor.lifterById(id).one();
    }

    @Override
    public List<Workout> workoutsByLifter(UUID lifterId) {
        return this.accessor.workoutsByLifter(lifterId).all();
    }

    @Override
    public List<Lift> liftsByWorkout(UUID workoutId) {
        return this.accessor.liftsByWorkout(workoutId).all();
    }

    @Override
    public void store(Workout workout) {
        this.workoutMapper.save(workout);
    }

    @Override
    public Lift addLiftToWorkout(UUID workoutId, String description, int sets, int reps, double weight) throws WorkoutFinishedException {
        Workout workout = this.accessor.workoutById(workoutId).one();
        Lifter lifter = this.accessor.lifterById(workout.getLifterId()).one();

        if(workout.isCompleted()) {
            throw new WorkoutFinishedException(workoutId);
        }

        ResultSet resultSet = this.session.execute("SELECT MAX(idx) FROM lifts_by_workout WHERE workout_id = ?", workoutId);
        Integer maxIndex = resultSet.one().get(0, Integer.class);
        maxIndex = maxIndex == null ? 0 : maxIndex;

        Lift lift = new Lift();
        lift.setWorkoutId(workoutId);
        lift.setDescription(description);
        lift.setSets(sets);
        lift.setReps(reps);
        lift.setWeight(weight);
        lift.setIndex(maxIndex + 1);

        this.liftMapper.save(lift);
        this.leaderBoardApi.recordLift(lifter, lift);

        return lift;
    }

    @Accessor
    private interface LogBookAccessor {
        @Query("SELECT * FROM lifters_by_email")
        Result<Lifter> allLifters();

        @Query("SELECT * FROM lifters_by_id WHERE lifter_id = :lifterId")
        Result<Lifter> lifterById(@Param("lifterId") UUID lifterId);

        @Query("SELECT * FROM workouts_by_lifter WHERE lifter_id = :lifterId")
        Result<Workout> workoutsByLifter(@Param("lifterId") UUID lifterId);

        @Query("SELECT * FROM workouts_by_id WHERE workout_id = :workoutId")
        Result<Workout> workoutById(@Param("workoutId") UUID workoutId);

        @Query("SELECT * FROM lifts_by_workout WHERE workout_id = :workoutId")
        Result<Lift> liftsByWorkout(@Param("workoutId") UUID workoutId);
    }
}

