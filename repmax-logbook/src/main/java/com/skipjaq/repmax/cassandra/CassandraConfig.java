package com.skipjaq.repmax.cassandra;

import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.Session;
import com.skipjaq.repmax.LeaderBoardApi;
import com.skipjaq.repmax.LogBookService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CassandraConfig {

    @Bean
    public Cluster cluster(@Value("${cassandra.contact-points}") String[] contactPoints) {
        Cluster cluster = Cluster.builder().addContactPoints(contactPoints).build();
        return cluster;
    }

    @Bean
    public Session session(Cluster cluster, @Value("${cassandra.keyspace-name}") String keyspaceName) {
        return cluster.connect(keyspaceName);
    }

    @Bean
    public LogBookService logBookService(Session session, LeaderBoardApi leaderBoardApi) {
        return new CassandraLogBookService(session, leaderBoardApi);
    }
}
