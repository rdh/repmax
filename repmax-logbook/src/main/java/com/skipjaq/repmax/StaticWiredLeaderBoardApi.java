package com.skipjaq.repmax;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

/**
 * @author Rob Harrop
 */
@Component
@Profile("default")
public class StaticWiredLeaderBoardApi extends AbstractLeaderBoardApi {

    @Override
    protected String getLeaderBoardAddress() {
        return "http://localhost:8082";
    }
}
