package com.skipjaq.repmax;

import com.skipjaq.repmax.model.Lift;
import com.skipjaq.repmax.model.Lifter;

/**
 * @author Rob Harrop
 */
public interface LeaderBoardApi {

    void recordLift(Lifter lifter, Lift lift);
}
