package com.skipjaq.repmax.model;

import com.datastax.driver.mapping.annotations.Column;
import com.datastax.driver.mapping.annotations.PartitionKey;
import com.datastax.driver.mapping.annotations.Table;
import lombok.Data;

import java.util.Date;
import java.util.UUID;

/**
 * @author Rob Harrop
 */
@Table(name = "workouts_by_lifter")
@Data
public class Workout {

    @PartitionKey
    @Column(name = "lifter_id")
    private UUID lifterId;

    @Column(name = "started_at")
    private Date startedAt;

    @Column(name = "description")
    private String description;

    @Column(name = "completed_at")
    private Date completedAt;

    @Column(name = "workout_id")
    private UUID workoutId;

    public static Workout newWorkout() {
        Workout workout = new Workout();
        workout.setWorkoutId(UUID.randomUUID());
        workout.setStartedAt(new Date());
        return workout;
    }

    public void complete() {
        this.completedAt = new Date();
    }

    public boolean isCompleted() {
        return this.completedAt != null;
    }
}
