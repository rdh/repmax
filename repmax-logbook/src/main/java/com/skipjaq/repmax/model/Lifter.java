package com.skipjaq.repmax.model;

import com.datastax.driver.mapping.annotations.Column;
import com.datastax.driver.mapping.annotations.PartitionKey;
import com.datastax.driver.mapping.annotations.Table;
import lombok.Data;

import java.util.Date;
import java.util.UUID;

/**
 * @author Rob Harrop
 */
@Table(name = "lifters_by_email")
@Data
public final class Lifter {

    @PartitionKey
    @Column(name = "email_address")
    private String emailAddress;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "joined_At")
    private Date joinedAt;

    @Column(name = "lifter_id")
    private UUID id;

    public String getFullName() {
        return this.firstName + " " + this.lastName;
    }

    public static Lifter newLifter(String emailAddress, String firstName, String lastName) {
        Lifter lifter = new Lifter();
        lifter.setEmailAddress(emailAddress);
        lifter.setFirstName(firstName);
        lifter.setLastName(lastName);
        lifter.setJoinedAt(new Date());
        lifter.setId(UUID.randomUUID());
        return lifter;
    }
}
