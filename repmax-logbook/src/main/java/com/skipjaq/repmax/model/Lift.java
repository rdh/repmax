package com.skipjaq.repmax.model;

import com.datastax.driver.mapping.annotations.Column;
import com.datastax.driver.mapping.annotations.PartitionKey;
import com.datastax.driver.mapping.annotations.Table;
import lombok.Data;

import java.util.Date;
import java.util.UUID;

/**
 * @author Rob Harrop
 */
@Table(name = "lifts_by_workout")
@Data
public class Lift {

    @PartitionKey
    @Column(name = "workout_id")
    private UUID workoutId;

    @Column(name = "idx")
    private int index;

    private String description;

    private int sets;

    private int reps;

    private double weight;

}
