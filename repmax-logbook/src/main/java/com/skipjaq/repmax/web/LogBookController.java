package com.skipjaq.repmax.web;

import com.skipjaq.repmax.LogBookService;
import com.skipjaq.repmax.model.Lift;
import com.skipjaq.repmax.model.Workout;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Resources;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author Rob Harrop
 */
@Controller
public class LogBookController {

    private final LogBookService logBook;

    @Autowired
    public LogBookController(LogBookService logBook) {
        this.logBook = logBook;
    }

    @RequestMapping(value = "/lifters", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public HttpEntity<Resources<LifterResource>> show() {
        Stream<LifterResource> resources = this.logBook.allLifters().stream().map(LifterResource::new);
        return new HttpEntity<>(new Resources<>(resources.collect(Collectors.toList())));
    }

    @RequestMapping(value = "/lifters/{uuid}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public HttpEntity<LifterResource> show(@PathVariable UUID uuid) {
        return new HttpEntity<>(new LifterResource(this.logBook.lifterById(uuid)));
    }

    @RequestMapping(value = "/lifters/{uuid}/workouts", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public HttpEntity<Resources<WorkoutResource>> lifterWorkouts(@PathVariable UUID uuid) {
        Stream<WorkoutResource> workouts = this.logBook.workoutsByLifter(uuid).stream().map(WorkoutResource::new);
        return new HttpEntity<>(new Resources<>(workouts.collect(Collectors.toList())));
    }

    @RequestMapping(value = "/workouts/{uuid}/lifts", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public HttpEntity<Resources<Lift>> workoutLifts(@PathVariable UUID uuid) {
        List<Lift> lifts = this.logBook.liftsByWorkout(uuid);
        return new HttpEntity<>(new Resources<>(lifts));
    }

    @RequestMapping(value = "/workouts/{uuid}/lifts", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> addNewLift(@PathVariable UUID uuid, @RequestBody LiftData liftData) {
        HttpStatus status;
        try {
            this.logBook.addLiftToWorkout(uuid, liftData.getDescription(), liftData.getSets(), liftData.getReps(), liftData.getWeight());
            status = HttpStatus.CREATED;
        } catch (LogBookService.WorkoutFinishedException e) {
            status = HttpStatus.METHOD_NOT_ALLOWED;
        }

        return new ResponseEntity<Void>(status);
    }


}
