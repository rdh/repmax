package com.skipjaq.repmax.web;

import com.skipjaq.repmax.model.Lifter;
import org.apache.commons.logging.Log;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.ResourceSupport;

import java.util.Date;
import java.util.UUID;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

/**
 * @author Rob Harrop
 */

public class LifterResource extends ResourceSupport {

    private final Lifter lifter;

    public LifterResource(Lifter lifter) {
        this.lifter = lifter;
        add(linkTo(methodOn(LogBookController.class).show(lifter.getId())).withSelfRel());
        add(linkTo(methodOn(LogBookController.class).lifterWorkouts(lifter.getId())).withRel("workouts"));
    }

    public UUID getLifterId() {
        return lifter.getId();
    }

    public Date getJoinedAt() {
        return lifter.getJoinedAt();
    }

    public String getLastName() {
        return lifter.getLastName();
    }

    public String getFirstName() {
        return lifter.getFirstName();
    }

    public String getEmailAddress() {
        return lifter.getEmailAddress();
    }
}
