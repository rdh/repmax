package com.skipjaq.repmax.web;

import com.skipjaq.repmax.model.Workout;
import org.springframework.hateoas.ResourceSupport;

import java.util.Date;
import java.util.UUID;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

/**
 * @author Rob Harrop
 */
public class WorkoutResource extends ResourceSupport {

    private final Workout workout;

    public WorkoutResource(Workout workout) {
        this.workout = workout;
        add(linkTo(methodOn(LogBookController.class).workoutLifts(workout.getWorkoutId())).withRel("lifts"));
        add(linkTo(methodOn(LogBookController.class).show(workout.getLifterId())).withRel("lifter"));
    }

    public UUID getWorkoutId() {
        return workout.getWorkoutId();
    }

    public Date getCompletedAt() {
        return workout.getCompletedAt();
    }

    public String getDescription() {
        return workout.getDescription();
    }

    public Date getStartedAt() {
        return workout.getStartedAt();
    }

    public UUID getLifterId() {
        return workout.getLifterId();
    }
}
