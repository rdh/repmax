package com.skipjaq.repmax.web;

import lombok.Data;

/**
 * @author Rob Harrop
 */
@Data
public class LiftData {

    private String description;

    private int sets;

    private int reps;

    private double weight;
}
