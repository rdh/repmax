package com.skipjaq.repmax;

import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

/**
 * @author Rob Harrop
 */
@Configuration
@EnableEurekaClient
@Profile("discovery")
public class EurekaConfig {

}
