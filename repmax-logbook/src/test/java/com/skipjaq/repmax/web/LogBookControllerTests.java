package com.skipjaq.repmax.web;

import com.skipjaq.repmax.LogBookService;
import com.skipjaq.repmax.model.Lift;
import com.skipjaq.repmax.model.Lifter;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * @author Rob Harrop
 */
@RunWith(SpringRunner.class)
@WebMvcTest(LogBookController.class)
@Import(TestWebConfiguration.class)
public class LogBookControllerTests {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private LogBookService logBookService;

    private JacksonTester<LiftData> json;

    @Test
    public void getLifters() throws Exception {
        List<Lifter> testLifters = testLifters();
        given(this.logBookService.allLifters()).willReturn(testLifters);

        this.mvc.perform(get("/lifters").accept(MediaType.APPLICATION_JSON_UTF8)).andDo(print(System.err))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$._embedded.lifterResourceList", content()).isNotEmpty())
                .andExpect(jsonPath("$._embedded.lifterResourceList[0].lifterId", content()).value(testLifters.get(0).getId().toString()));
    }

    @Test
    public void getLifter() throws Exception {
        Lifter rob = rob();
        given(this.logBookService.lifterById(rob.getId())).willReturn(rob);

        this.mvc.perform(get("/lifters/{uuid}", rob.getId()).accept(MediaType.APPLICATION_JSON)).andDo(print(System.err))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.lifterId", content()).value(rob.getId().toString()))
                .andExpect(jsonPath("$._links['self']", content()).isNotEmpty())
                .andExpect(jsonPath("$._links['workouts']", content()).isNotEmpty());
    }

    @Test
    public void addLift() throws Exception {
        UUID uuid = UUID.randomUUID();

        String json = "{\n" +
                "  \"description\": \"Back Squat\",\n" +
                "  \"sets\": 5,\n" +
                "  \"reps\": 10,\n" +
                "  \"weight\": 65\n" +
                "}";

        given(this.logBookService.addLiftToWorkout(uuid, "Back Squat", 5, 10, 65)).willReturn(new Lift());

        this.mvc.perform(post("/workouts/{uuid}/lifts", uuid)
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(status().is2xxSuccessful());
    }

    private List<Lifter> testLifters() {
        List<Lifter> lifters = new ArrayList<>();
        lifters.add(rob());
        lifters.add(josh());
        return lifters;
    }

    private Lifter josh() {
        return Lifter.newLifter("josh.long@pivotal.com", "Josh", "Long");
    }

    private Lifter rob() {
        return Lifter.newLifter("rob@skipjaq.com", "Rob", "Harrop");
    }
}
