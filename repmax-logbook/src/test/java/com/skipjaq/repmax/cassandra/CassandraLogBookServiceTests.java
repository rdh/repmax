package com.skipjaq.repmax.cassandra;

import com.skipjaq.repmax.LeaderBoardApi;
import com.skipjaq.repmax.LogBookService;
import com.skipjaq.repmax.model.Lift;
import com.skipjaq.repmax.model.Lifter;
import com.skipjaq.repmax.model.Workout;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.UUID;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = CassandraConfig.class, webEnvironment = SpringBootTest.WebEnvironment.NONE)
public class CassandraLogBookServiceTests {

    private static final UUID ROB_HARROP_UUID = UUID.fromString("a2046997-ed93-483d-8b06-96f8d17f916c");

    private static final UUID JOSH_LONG_UUID = UUID.fromString("bb8ad3d9-f998-474b-ad2d-3dc0084aae8f");

    private static final UUID TEST_WORKOUT_UUID = UUID.fromString("b090c527-8a4b-4064-b920-372c4a5b9c80");

    @Autowired
    CassandraLogBookService repository;

    @MockBean
    LeaderBoardApi leaderBoardApi;

    @Test
    public void allLifters() {
        List<Lifter> lifters = this.repository.allLifters();
        assertEquals(2, lifters.size());

        // lifters should be sorted
        Lifter lifter = lifters.get(0);

        assertIsRob(lifter);
        assertNotNull(lifter.getJoinedAt());
    }

    @Test
    public void lifterById() {
        Lifter lifter = this.repository.lifterById(ROB_HARROP_UUID);
        assertIsRob(lifter);
    }

    @Test
    public void lifterWorkouts() {
        List<Workout> workouts = workoutsByLifter(ROB_HARROP_UUID);
        assertEquals(2, workouts.size());
        Workout workout = workouts.get(0);
        assertEquals("Back Squat 1", workout.getDescription());
    }


    @Test
    public void lifterWorkoutsSortedByStartedAt() {
        List<Workout> workouts = workoutsByLifter(ROB_HARROP_UUID);
        List<Workout> copy = new ArrayList<>(workouts);
        copy.sort(Comparator.comparing(Workout::getStartedAt));
        assertEquals(copy, workouts);

    }

    @Test
    public void liftsByWorkout() {
        List<Lift> lifts = this.repository.liftsByWorkout(TEST_WORKOUT_UUID);
        assertNotNull(lifts);
        assertFalse(lifts.isEmpty());
        assertEquals(5, lifts.size());
    }

    @Test
    public void storeNewWorkout() {
        int before = countWorkouts(JOSH_LONG_UUID);
        createAndStoreNewWorkout();
        assertEquals(before + 1, countWorkouts(JOSH_LONG_UUID));
    }

    @Test
    public void storeLiftForWorkout() throws Exception {
        Workout workout = createAndStoreNewWorkout();
        assertEquals(0, this.repository.liftsByWorkout(workout.getWorkoutId()).size());
        Lift lift = this.repository.addLiftToWorkout(workout.getWorkoutId(), "Back Squat", 5, 10, 67.5);
        assertNotNull(lift);
        assertEquals(1, this.repository.liftsByWorkout(workout.getWorkoutId()).size());
    }

    @Test(expected = LogBookService.WorkoutFinishedException.class)
    public void storeLiftForClosedWorkout() throws Exception {
        Workout workout = createAndStoreNewWorkout();
        workout.complete();
        this.repository.store(workout);
        this.repository.addLiftToWorkout(workout.getWorkoutId(), "Back Squat", 5, 10, 67.5);
    }

    private void assertIsRob(Lifter lifter) {
        assertNotNull(lifter);
        assertEquals("Rob", lifter.getFirstName());
        assertEquals("Harrop", lifter.getLastName());
        assertEquals("rob@skipjaq.com", lifter.getEmailAddress());
    }

    private Workout createAndStoreNewWorkout() {
        Workout workout = Workout.newWorkout();
        workout.setLifterId(JOSH_LONG_UUID);
        this.repository.store(workout);
        return workout;
    }

    private List<Workout> workoutsByLifter(UUID lifterId) {
        return this.repository.workoutsByLifter(lifterId);
    }

    private int countWorkouts(UUID lifterId) {
        return workoutsByLifter(lifterId).size();
    }
}
