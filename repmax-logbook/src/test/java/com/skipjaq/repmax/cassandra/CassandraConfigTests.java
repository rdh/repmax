package com.skipjaq.repmax.cassandra;

import com.datastax.driver.core.Session;
import com.skipjaq.repmax.LeaderBoardApi;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = CassandraConfig.class, webEnvironment = SpringBootTest.WebEnvironment.NONE)
public class CassandraConfigTests {

    @MockBean
    LeaderBoardApi leaderBoardApi;

    @Autowired
    Session session;

    @Test
    public void configIsCorrect() {
        assertNotNull(this.session);
    }

}
